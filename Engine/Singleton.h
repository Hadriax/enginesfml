#pragma once
#include<memory>

namespace game
{
	template <typename T>
	class Singleton
	{
	public:
		// Singleton Instance Getter
		static std::shared_ptr<T> Get();
		static int GetRefCount();
	protected:
		Singleton() { }

		// Deleted methods
		Singleton(const T&) = delete;
		Singleton& operator= (const T) = delete;
	};

	template <typename T>
	int Singleton<T>::GetRefCount()
	{
		return Get().use_count();
	}

	template<typename T>
	std::shared_ptr<T> Singleton<T>::Get()
	{
		// Local statics will first be initialized when the function is called the first time, It is Thread Safe
		static const std::shared_ptr<T> instance = std::make_shared<T>();
		return instance;
	}
}
