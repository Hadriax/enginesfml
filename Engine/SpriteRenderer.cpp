#include "SpriteRenderer.h"
#include "Actor.h"

namespace game
{
	SpriteRenderer::SpriteRenderer()
	{
	}

	void SpriteRenderer::SetSprite(const std::shared_ptr<sf::Texture>& _tx)
	{
		texture = _tx;
		sprite.setTexture(*_tx);
		sprite.setTextureRect(
			sf::IntRect(
				sf::Vector2i(),
				sf::Vector2i((*_tx).getSize())
			)
		);

		box.setPosition(sprite.getPosition());
		sf::Vector2f size(sprite.getGlobalBounds().width, sprite.getGlobalBounds().height);
		box.setSize(size);
		box.setOutlineColor(sf::Color::Red);
		box.setOutlineThickness(1.f);
		box.setFillColor(sf::Color::Transparent);
	}

	void SpriteRenderer::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(sprite, states);

		if (drawBox)
			target.draw(box, states);
	}

	void SpriteRenderer::Update(float deltaTime)
	{
		sprite.setPosition(owner->GetPosition());

		if (drawBox)
			box.setPosition(sprite.getPosition());
	}
}