#include "HMaths.h"

/*
#include <iostream>
#include <time.h>
#include <math.h>
#include <vector>
#include <array>

using namespace std;

vector<long> recordedFactoriels;
std::array<long, 20> recordedFactorielsArray;

void init_custom_factoriel()
{
	if (recordedFactoriels.size() == 0)
	{
		
		long v = 1;
		recordedFactoriels.push_back(1);
		for (int i = 1; i <= 20; i++)
		{
			v *= i;
			recordedFactorielsArray[i] = v;
			recordedFactoriels.push_back(v);
		}
	}
}

long long custom_factoriel(unsigned int order)
{
	if (order > 20) return -1;
	return recordedFactorielsArray[order];
}

// 0 -> sin
// 1 -> cos
// 2 -> -sin
// 3 -> -cos
// 4(0) -> sin
// etc ...
double derivative_sin(unsigned int order)
{
	unsigned int val = order % 4;
	switch (val)
	{
	case 0:
		// sin
		return 0;
		break;
	case 1:
		// cos
		return 1;
		break;
	case 2:
		// -sin
		return 0;
		break;
	case 3:
		// -cos
		return -1;
		break;
	default:
		return -999;
		break;
	}
}

// 0 -> cos
// 1 -> -sin
// 2 -> -cos
// 3 -> sin
// 4(0) -> cos
// etc ...
double derivative_cos(unsigned int order)
{
	unsigned int val = order % 4;
	switch (val)
	{
	case 0:
		// cos
		return 1;
		break;
	case 1:
		// -sin
		return 0;
		break;
	case 2:
		// -cos
		return -1;
		break;
	case 3:
		// sin
		return 0;
		break;
	default:
		return -999;
		break;
	}
}

double custom_pow(double value, unsigned int pow)
{
	double result = 1;
	for (unsigned int i = 1; i < pow + 1; i++)
	{
		result *= value;
	}
	return result;
}

double polynome_sin(double h, unsigned int order)
{
	double result = 0;
	double h2 = h * h;
	for (int i = 1; i < order + 1; i += 2)
	{
		result += (custom_pow(h, i) / custom_factoriel(i)) * derivative_sin(i);
	}
	
	return result;
}

int main()
{
	int interations = 1000000;
	double h = 0.5;
	init_custom_factoriel();

	clock_t begin_custom, end_custom, total_custom;
	begin_custom = clock();
	for (int i = 0; i < interations; i++)
	{
		polynome_sin(h, 3);
	}
	end_custom = clock();
	total_custom = (double)((end_custom - begin_custom));
	cout << total_custom << endl;

	begin_custom = clock();
	for (int i = 0; i < interations; i++)
	{
		sin(h);
	}
	end_custom = clock();
	total_custom = (double)((end_custom - begin_custom));
	cout << total_custom << endl;
}
*/