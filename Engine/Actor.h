#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Graphics\RenderStates.hpp>
#include <type_traits>
#include <memory>
#include "Component.h"

namespace game
{
	class Actor : public sf::Drawable
	{
	protected:
		std::vector<std::unique_ptr<Component>> components;
		sf::Vector2f position;

	public:
		Actor();

	protected:
		void draw(sf::RenderTarget& target, sf::RenderStates state = sf::RenderStates::Default) const;

	public:
		inline const sf::Vector2f& GetPosition() { return position; };

		void Update(float deltaTime);

		template<typename T>
		T* AddComponent()
		{
			static_assert(std::is_base_of_v<Component, T>, "T must derive from from Component");

			components.push_back(std::make_unique<T>());

			Component* ptr = components.at(components.size() - 1).get();

			ptr->SetOwner(this);

			return dynamic_cast<T*>(ptr);
		}

		template<typename T>
		T* GetComponent()
		{
			for (auto it = components.begin(); it != components.end(); ++it)
			{
				T* castedCompo = dynamic_cast<T*>((*it).get());

				if (castedCompo != nullptr)
					return castedCompo;
			}
			return nullptr;
		}
	};


}