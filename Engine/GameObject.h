#pragma once
#include "Component.h"

namespace game
{
	class GameObject : public sf::Drawable
	{
	protected:
		std::vector<Component*> components;

	public:
		GameObject();

	protected:
		void draw(sf::RenderTarget& target, sf::RenderStates state = sf::RenderStates::Default) const;

	public:
		// template<typename T>
		// void AddComponent();

		// (template<typename T>
		// T* GetComponent();
	};


}