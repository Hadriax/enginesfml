#include "Component.h"
#include "Actor.h"

void game::Component::SetOwner(Actor* newOwner)
{
	owner = newOwner;
}

void game::Component::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
}
