#pragma once
#include <memory>
#include <type_traits>
#include "ILoadableFromFile.h"

namespace game
{
	// Asset loadable from disk. T is the encapsulated ressource. For exemple sf::Texture is a valid encapsulated type.
	// ChildClass must implement ILoadableFromFile.
	template<typename T, typename ChildClass>
	class Asset : public ILoadableFromFile
	{
	public:
		explicit Asset(T& data);
		explicit Asset(std::string _path);
		// explicit Asset(const T& _source);

	protected:
		// Pointer to the loaded asset.
		std::shared_ptr<T> data;

	public:
		T* GetData();

		// Load the data from file.
		void Load(std::string _path);

		// Get a full copy of the source asset.
		T Copy(const T& _source);

		const T* GetDataReadonly();


		// Inherited via ILoadableFromFile
		virtual bool LoadFromFile(std::string _path) = 0;

		/*
		template<typename T>
		void SetData(T* _data)
		{
			data.reset(_data);
		}

		template<typename T>
		T* GetData()
		{
			return data->get();
		}
		*/
	};

	/*
	template<typename T, typename ChildClass>
	inline Asset<T, ChildClass>::Asset(T& _data)
	{
		static_assert(std::is_base_of_v<ILoadableFromFile, ChildClass>, "Child Class must implement ILoadableFromFile.");
		data = std::make_shared<T>(_data);

		//dynamic_cast<ILoadableFromFile>(_data.get()).LoadFromFile()
	}

	template<typename T, typename ChildClass>
	inline Asset<T, ChildClass>::Asset(std::string _path)
	{
		static_assert(std::is_base_of_v<ILoadableFromFile, ChildClass>, "Child Class must implement ILoadableFromFile.");

		data = std::make_shared<T>(T());
		
		LoadFromFile(_path);
	}

	template<typename T, typename ChildClass>
	inline T* Asset<T, ChildClass>::GetData()
	{
		return data.get();
	}
	*/
}



