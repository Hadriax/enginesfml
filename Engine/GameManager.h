#pragma once
#include <SFML\Graphics.hpp>
#include <memory>
#include <iostream>
#include "Singleton.h"
#include "Screen.h"
#include "Game.h"

namespace game
{
	class GameManager : public Singleton<GameManager>
	{
	private:
		Screen* screen;
		Game* game = nullptr;

	public:		
		inline void RegisterEntity(Actor* entity) { game->RegisterEntity(entity); }
		inline Screen* GetScreen() { return screen; }
		void SetGame(Game* game);
	};
}

