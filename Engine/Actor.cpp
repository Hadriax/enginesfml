#include "Actor.h"
#include "GameManager.h"
#include "Component.h"

namespace game
{
	Actor::Actor()
	{
		GameManager::Get()->RegisterEntity(this);
	}

	void Actor::draw(sf::RenderTarget& target, sf::RenderStates state) const
	{
		for (auto it = components.begin(); it != components.end(); ++it)
		{
			target.draw(*(*it), state);
		}
	}

	void Actor::Update(float deltaTime)
	{
		for (auto it = components.begin(); it != components.end(); ++it)
		{
			(*it)->Update(deltaTime);
		}
	}

}

