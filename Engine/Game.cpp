#include "Game.h"
#include "GameManager.h"

namespace game
{
	Game::Game(int screenWidth, int screenHeight)
	{
		GameManager::Get()->SetGame(this);
		screen = std::make_shared<Screen>(Screen(screenWidth, screenHeight));
		window = screen->GetWindow();
		window->setTitle(name);
	}

	void Game::Awake()
	{
	}

	void Game::Start()
	{
	}

	void Game::Update()
	{
		// Delta Time computation here
		float deltaTime = 0.f;
		//

		for (auto it = entites.begin(); it != entites.end(); ++it)
		{
			if (*it == nullptr)
				std::cout << "Registered entity is nullprt in Game." << std::endl;

			(*it)->Update(deltaTime);
		}
	}

	void Game::Render()
	{
		window->clear();

		for (auto it = entites.begin(); it != entites.end(); ++it)
		{
			if (*it == nullptr)
				std::cout << "Registered entity is nullprt in Game." << std::endl;

			window->draw(* (*it));
		}

		window->display();
	}

	void Game::GameLoop()
	{
		Awake();
		Start();

		while (window->isOpen())
		{
			sf::Event event;
			while (window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					window->close();
			}

			Update();
			Render();

		}
	}
}