#pragma once
#include "Component.h"
#include <memory>

namespace game
{
	class SpriteRenderer : public Component
	{

	private:
		sf::Sprite sprite;
		std::shared_ptr<sf::Texture> texture;
		sf::RectangleShape box;
			   
	public:
		bool drawBox = true;

	public:
		SpriteRenderer();
		void SetSprite(const std::shared_ptr<sf::Texture>& _tx);

	protected:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		// Inherited via Component
		virtual void Update(float deltaTime) override;
	};

}

