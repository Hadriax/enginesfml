#pragma once
#include "Screen.h"
#include "Actor.h"
#include <iostream>

namespace game
{
	class Game
	{
		const std::string name = "Game";

		std::shared_ptr<Screen> screen;
		sf::RenderWindow* window;

		std::vector<Actor*> entites;

	public:
		Game(int screenWidth = 800, int screenHeight= 600);

		inline std::shared_ptr<Screen> GetScreen() { return screen; }
		inline void RegisterEntity(Actor* entity) { entites.push_back(entity); }
		void GameLoop();

	private:
		void Awake();
		void Start();
		void Update();
		void Render();
	};
}