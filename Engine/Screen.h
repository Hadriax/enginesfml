#pragma once
#include <SFML\Graphics.hpp>
#include <memory>

namespace game
{
	// Contain the main datas related to Display and the RenderWindow
	class Screen
	{
		int width = 800;
		int height = 600;

		sf::RenderWindow* window;

	public:
		Screen(int _width = 800, int _height = 600);
		sf::RenderWindow* GetWindow() { return window; };
	};
}

