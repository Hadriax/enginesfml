#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Graphics\RenderStates.hpp>

namespace game
{
	class Actor;
	class Component : public sf::Drawable
	{
	protected:
		Actor* owner;

	public:
		//Component(const Component& other) = default;
		//Component& operator=(const Component & other) = default;

		void SetOwner(Actor* newOwner);

	protected:
		// Inherited via Drawable
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	public:
		virtual void Update(float deltaTime) = 0;
	};
}