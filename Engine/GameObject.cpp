#include "GameObject.h"
#include "GameManager.h"
#include <type_traits>

namespace game
{
	GameObject::GameObject()
	{
		GameManager::Get()->RegisterEntity(this);
	}

	void GameObject::draw(sf::RenderTarget& target, sf::RenderStates state) const
	{
		for (auto* component : components)
		{
			target.draw(*component, state);
		}
	}

	/*
	template<typename T>
	void GameObject::AddComponent()
	{
		static_assert(std::is_base_of_v<Component, T>, "T must derive from from Component");

		components.push_back(new T());
	}
	*/

	/*
	template<typename T>
	T* GameObject::GetComponent()
	{
		for (auto* component : components)
		{
			T castedCompo = dynamic_cast<T>(component);

			if (T != nullptr_t)
				return castedCompo;
		}
		return nullptr;
	}
	*/

}

