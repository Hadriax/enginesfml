#include "Screen.h"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

namespace game
{
	Screen::Screen(int _width, int _height)
	{
		this->width = _width;
		this->height = _height;

		window = new sf::RenderWindow;
		window->create(sf::VideoMode(_width, _height), "Game");
	}
}