#include "TextureMemory.h"

namespace game
{
	std::shared_ptr<sf::Texture> TextureMemory::LoadTexture(const std::string _txName)
	{
		auto tx = LoadedTextures.find(_txName);
		// If already loaded.
		if (LoadedTextures.find(_txName) != LoadedTextures.cend())
		{
			return std::make_shared<sf::Texture>(*tx->second);
		}
		// Load from File.
		else
		{
			// Create sf::Texture pointer.
			sf::Texture* txPtr = new sf::Texture();
			txPtr->loadFromFile(_txName);

			// Give pointer ownership to a shared_ptr and insert into the loaded textures map.
			auto t = LoadedTextures.emplace(_txName, std::make_shared<sf::Texture>(*txPtr));
			
			bool bEmplaceSuccessfull = t.second;

			if (bEmplaceSuccessfull)
			{
				// Dereference iterator and get the pointer from the std::pair.
				return t.first->second;
			}			
		}

		return nullptr;
	}
}