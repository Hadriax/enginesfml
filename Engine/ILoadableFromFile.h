#pragma once
#include <string>

class ILoadableFromFile
{
public:
	virtual bool LoadFromFile(std::string _path) = 0;

};

