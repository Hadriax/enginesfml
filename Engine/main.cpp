#include "Game.h"
#include "GameManager.h"
#include "SpriteRenderer.h"
#include "TextureMemory.h"

using namespace game;

int main()
{
	Game game;
	TextureMemory txMem;
	// sf::Texture texture;
	// texture.loadFromFile("../Assets/Sprites/Drone.png");

	Actor actor;
	Actor actor1;

	SpriteRenderer* s = actor.AddComponent<SpriteRenderer>();
	s->SetSprite(txMem.LoadTexture("../Assets/Sprites/Drone.png"));

	SpriteRenderer* s1 = actor1.AddComponent<SpriteRenderer>();
	s1->SetSprite(txMem.LoadTexture("../Assets/Sprites/Drone.png"));
	
	game.GameLoop();
	
	return 0;
}	