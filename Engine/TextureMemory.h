#pragma once
#include "Asset.h"

// STL
#include <map>
#include <string>
#include <memory>

// SFML
#include <SFML/Graphics/Texture.hpp>

namespace game
{
	class TextureMemory
	{
		// Map containing every currently loaded Texture.
		std::map<std::string, std::shared_ptr<sf::Texture>> LoadedTextures;
	public:
		// Check in map if texture already loaded, else load it from file and emplace it in the LoadedTextures.
		std::shared_ptr<sf::Texture> LoadTexture(const std::string _txName);
	};

}